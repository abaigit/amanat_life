from django.db import models
from django.contrib.auth import get_user_model
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation, GenericForeignKey
from ckeditor.fields import RichTextField
from crowdfund.models import (DiseaseTag as Tag, Image)

User = get_user_model()


class Post(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    content = RichTextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    cover = models.ImageField(upload_to='posts/cover', null=True, blank=True)
    tags = models.ManyToManyField(Tag, related_name='post_tags', default=None, blank=True)
    gallery = GenericRelation(Image, related_query_name='posts_gallery')

    def __str__(self):
        return self.title


class Comment(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    content = RichTextField()
    timestamp = models.DateTimeField(auto_now_add=True)
    author = models.ForeignKey(User, on_delete=models.CASCADE, related_name='comments')

    def __str__(self):
        return f"Comment to «{self.content_object}»"
