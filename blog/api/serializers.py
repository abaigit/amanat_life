from django.contrib.contenttypes.models import ContentType
from rest_framework import serializers
from blog.models import (Post, Comment)
from account.api.serializers import UserBaseSerializer


class PostSerializer(serializers.ModelSerializer):
    author = UserBaseSerializer()
    comments = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = "__all__"
        depth = 1

    def get_comments(self, obj):
        ct = ContentType.objects.get_for_model(Post)
        comments = Comment.objects.filter(content_type=ct, object_id=obj.id)
        return CommentSerializer(instance=comments, many=True).data


class CommentSerializer(serializers.ModelSerializer):
    author = UserBaseSerializer()

    class Meta:
        model = Comment
        fields = '__all__'
