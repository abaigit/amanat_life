from django.contrib.contenttypes.models import ContentType
from rest_framework.permissions import IsAuthenticatedOrReadOnly
from rest_framework.response import Response
from rest_framework import status
from rest_framework.generics import ListAPIView, CreateAPIView
from blog.models import Post, Comment
from .serializers import PostSerializer, CommentSerializer


class PostListAPIView(ListAPIView):
    serializer_class = PostSerializer
    queryset = Post.objects.all()


class PostCommentCreateAPIView(CreateAPIView):
    serializer_class = CommentSerializer
    permission_classes = [IsAuthenticatedOrReadOnly]

    def create(self, request, *args, **kwargs):
        data = request.data
        ct = ContentType.objects.get_for_model(Post)
        data.update({"author": request.user, "content_type": ct})
        comment = Comment.objects.create(**data)
        sr = self.serializer_class(instance=comment)
        return Response(status=status.HTTP_200_OK, data=sr.data)
