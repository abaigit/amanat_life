from django.urls import path
from . import views

app_name = 'blog-api'

urlpatterns = [
    path('post/list/', views.PostListAPIView.as_view(), name='list'),
    path('comment/add/', views.PostCommentCreateAPIView.as_view(), name='comment-add'),
]
