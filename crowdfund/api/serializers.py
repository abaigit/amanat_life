from rest_framework import serializers
from crowdfund.models import (DonationBox, Image, Document, Payment)
from account.api.serializers import UserBaseSerializer


class ImageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Image
        fields = ('id', 'file')


class DonationBoxSerializer(serializers.ModelSerializer):
    gallery = serializers.SerializerMethodField()
    recipient = serializers.SerializerMethodField()

    def get_gallery(self, instance):
        sr = ImageSerializer(instance.gallery, many=True)
        return sr.data

    def get_recipient(self, instance):
        return instance.recipient.get_fullname()

    class Meta:
        model = DonationBox
        fields = '__all__'


class PaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Payment
        fields = '__all__'
