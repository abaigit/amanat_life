from rest_framework.views import APIView
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.permissions import AllowAny
from crowdfund.models import (DonationBox, Image, Document, Payment)
from .serializers import DonationBoxSerializer, PaymentSerializer


class DonationBoxListAPIView(ListAPIView):
    serializer_class = DonationBoxSerializer
    queryset = DonationBox.objects.all()


class PaymentCreateView(CreateAPIView):
    serializer_class = PaymentSerializer
    queryset = Payment.objects.all()

    def create(self, request, *args, **kwargs):
        sr = self.serializer_class(data=request.data)
        if sr.is_valid(raise_exception=True):
            sr.save()
            d = DonationBox.objects.get(id=sr.data.get("box"))
            d.pledged += sr.data.get("amount")
            d.save()
        return Response(status=status.HTTP_200_OK, data=sr.data)


