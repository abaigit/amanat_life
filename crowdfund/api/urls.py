from django.urls import path
from . import views

app_name = 'crowdfund-api'

urlpatterns = [
    path('list/', views.DonationBoxListAPIView.as_view(), name='box-list'),
    path('payment/', views.PaymentCreateView.as_view(), name="payment"),
]
