from django.contrib import admin
from .models import (DonationBox, Image, Document, Payment, DiseaseTag)


@admin.register(DiseaseTag)
class DiseaseTagAdmin(admin.ModelAdmin):
    search_fields = ['name']


@admin.register(DonationBox)
class DonationBoxAdmin(admin.ModelAdmin):
    autocomplete_fields = ['tags']


admin.site.register(Image)
admin.site.register(Document)


@admin.register(Payment)
class PaymentAdmin(admin.ModelAdmin):
    list_display = ('box', 'amount', 'timestamp', 'user')
