from django.db import models
from django.contrib.auth import get_user_model
from django.core.validators import FileExtensionValidator
from django.contrib.contenttypes.fields import GenericForeignKey, GenericRelation
from django.contrib.contenttypes.models import ContentType
from ckeditor.fields import RichTextField
from .utils import image_save_path, file_save_path, cover_save_path

User = get_user_model()


class DiseaseTag(models.Model):
    name = models.CharField(max_length=250)

    def __str__(self):
        return self.name


class Image(models.Model):
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')
    file = models.ImageField(upload_to=image_save_path)


class DonationBox(models.Model):
    recipient = models.ForeignKey(User, on_delete=models.CASCADE)
    title = models.CharField(max_length=250)
    headline = models.CharField(max_length=550, null=True, blank=True, default=None)
    description = RichTextField()
    goal = models.IntegerField()
    pledged = models.IntegerField()
    cover = models.ImageField(upload_to=cover_save_path)
    reason = models.TextField()
    tags = models.ManyToManyField(DiseaseTag, related_name='dis_tags', default=None, blank=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    gallery = GenericRelation(Image)

    def __str__(self):
        return self.title


class Document(models.Model):
    box = models.ForeignKey(DonationBox, related_name='briefcase', on_delete=models.CASCADE)
    file = models.ImageField(upload_to=file_save_path,
                             validators=[FileExtensionValidator(allowed_extensions=['txt', 'pdf', 'csv'])])


class Payment(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True, default=None)
    box = models.ForeignKey(DonationBox, related_name='checks', on_delete=models.CASCADE)
    timestamp = models.DateTimeField(auto_now_add=True)
    amount = models.IntegerField()
    check_details = models.JSONField(null=True, blank=True)
