from django.db.models.query_utils import Q


def cover_save_path(instance, filename):
    file_type = filename.split('.')[-1]
    path = f"donation_box/cover/cover.{file_type}"
    return path


def image_save_path(instance, filename):    # TODO: refactor considering the content type
    file_type = filename.split('.')[-1]
    path = f"donation_box/gallery/{instance.content_object.id}_image.{file_type}"
    return path


def file_save_path(instance, filename):
    file_type = filename.split('.')[-1]
    path = f"donation_box/{instance.box.id}/documents/{instance.box.id}_filedoc.{file_type}"
    return path


def limit_patient_choices():
    return {"position": "PT"}


def limit_sponsor_choices():
    return {"position": "SP"}

