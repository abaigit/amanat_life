import { makeAutoObservable, runInAction } from "mobx";
import { csrftoken } from "./csrf_token";

const API = process.env.REACT_APP_API;

class Store {
    user = {}
    donationBoxes = []
    posts = []

    constructor() {
        makeAutoObservable(this);
        runInAction(() => {
            let localUser = localStorage.getItem("user");
            this.user = localUser ? JSON.parse(localUser) : {};

            let localBoxes = localStorage.getItem("boxes");
            this.donationBoxes = localBoxes ? JSON.parse(localBoxes) : [];
        });

        // fetch(API) // fetch initial patients
        //     .then(res => res.json())
        //     .then(data => {
        //         runInAction(() => {
        //             this.products = data;
        //         })
        //     });
    }

    async authenticate(credentials) {
        let authStatus = false;
        await fetch(`${API}/account/auth-token/`, {
            method: 'POST',
            body: JSON.stringify(credentials),
            headers: {
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            // mode: 'same-origin',
        }).then(res => res.json())
            .then(data => {
                this.user["token"] = data.token
                authStatus = this.user["token"] ? true : false;
            });
        return authStatus;
    }

    getUserData(email) {
        fetch(`${API}/account/details/`, {
            method: 'POST',
            body: JSON.stringify({ "email": email }),
            headers: {
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            // mode: 'same-origin',
        }).then(res => res.json())
            .then(data => {
                runInAction(() => {
                    this.user = { ...this.user, ...data }
                    localStorage.setItem("user", JSON.stringify(this.user));
                })
            });
    }

    updateUserData(userData) {
        fetch(`${API}/account/update/`, {
            method: 'PATCH',
            body: JSON.stringify(userData),
            headers: {
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
                'Authorization': `JWT ${store.user.token}`,
                'X-CSRFToken': csrftoken,
            },
            // mode: 'same-origin',
        }).then(res => res.json())
            .then(data => {
                runInAction(() => {
                    this.user = { ...this.user, ...data }  // возмжно тоже следует сделать прямое присваивание
                    localStorage.setItem("user", JSON.stringify(this.user));
                })
            });
    }

    async getDonationBoxes() {
        let rStatus = false;
        await fetch(`${API}/crowdfund/list/`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
                'X-CSRFToken': csrftoken,
            },
        })
            .then(res => res.json())
            .then(data => {
                runInAction(() => {
                    this.donationBoxes = data
                    localStorage.setItem("boxes", JSON.stringify(this.donationBoxes));
                    rStatus = true
                });
            });
        return rStatus;
    }

    async getPosts() {
        let rStatus = false;
        await fetch(`${API}/blog/post/list/`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
                'X-CSRFToken': csrftoken,
            },
        })
            .then(res => res.json())
            .then(data => {
                runInAction(() => {
                    this.posts = data
                    localStorage.setItem("posts", JSON.stringify(this.posts));
                    rStatus = true
                });
            });
        return rStatus;
    }

    async makeComment(dataDict) {
        fetch(`${API}/blog/comment/add/`, {
            method: "POST",
            body: JSON.stringify(dataDict),
            headers: {
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
                'Authorization': `JWT ${store.user.token}`,
                'X-CSRFToken': csrftoken,
            },
            // mode: 'same-origin',
        }).then(res => res.json())
            .then(data => {
                runInAction(() => {
                    this.getPosts();
                })
            });
    }

    logout() {
        this.user = {};
        localStorage.setItem("user", JSON.stringify(this.user));
    }


}

const store = new Store();

export default store;
