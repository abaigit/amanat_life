import React, { useState } from 'react';
import { Form, Input, Button, Card } from 'antd';
import { observer } from 'mobx-react';
import store from '../store/store';

const AccountUpdateForm = observer(() => {
    const [disabled, setDisabled] = useState(true);
    const onFinish = (formData) => {
        store.updateUserData(formData);
        setDisabled(true)
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);  // TODO: show modal with Error message
    };

    return (
        <Card size={"small"} style={{ maxWidth: "450px", margin: "auto" }}>
            <Form
                name="account-update-form"
                labelCol={{
                    span: 9,
                }}
                wrapperCol={{
                    span: 14,
                }}
                initialValues={store.user}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                autoComplete="on"
            >

                <Form.Item
                    label="Имя"
                    name="first_name"
                    rules={[
                        {
                            required: true,
                            message: 'Ваше имя!',
                        },
                    ]}
                >
                    <Input disabled={disabled} />
                </Form.Item>

                <Form.Item
                    label="Фамилия"
                    name="last_name"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Last Name!',
                        },
                    ]}
                >
                    <Input disabled={disabled} />
                </Form.Item>

                <Form.Item
                    label="Телефон"
                    name="phone_number"
                    rules={[
                        {
                            required: true,
                            message: 'Please input your Email!',
                        },
                    ]}
                >
                    <Input type={'number'} disabled={disabled} />

                </Form.Item>

                <Form.Item
                    label="Дата рождения"
                    name="birth_date"
                    rules={[
                        {
                            required: true,
                            message: 'Введите дату рождения!',
                        },
                    ]}
                >
                    <Input type={'date'} disabled={disabled} />

                </Form.Item>


                {disabled ?
                    <Button type="button" className='btn btn-primary my-3' onClick={() => setDisabled(false)}>
                        Изменить
                    </Button>
                    :
                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button type="primary" htmlType="submit" className='btn btn-primary my-3'>
                            Сохранить
                        </Button>
                    </Form.Item>
                }


            </Form>
        </Card>
    );
})

export default AccountUpdateForm;