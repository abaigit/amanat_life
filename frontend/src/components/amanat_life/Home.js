import React from 'react';
import { observer } from 'mobx-react';
import OurMissionBlock from './ourMission';
import DonatBoxSlider from './donatBoxSlider';
import StoryBanner from './storyBanner'
import store from '../../store/store';

const Home = observer(({ children }) => {
    return (
        <div>
            <OurMissionBlock />
            <DonatBoxSlider />
            {store.posts.length > 0 ? <StoryBanner /> : null}
        </div>
    )
});

export default Home;