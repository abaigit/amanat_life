import React, {useState} from 'react';
import Modal from './modal';
import picTwo from '../../assets/img/bg_2.jpg';

const BeVolunteerForm = () => {
    const [showModal, setShowModal] = useState(false);
    const modalClose = (bool) => { setShowModal(bool) };

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        const formValDict = Object.fromEntries(formData);
        e.target.reset();
        setShowModal(true);
        // sending email to managers . . . (using formValDict)
    }

    return (
        <div className="featured-section overlay-color-2" style={{ backgroundImage: `url(${picTwo})` }}>

            <div className="container">
                <div className="row">

                    <div className="col-md-6 mb-5 mb-md-0">
                        <img src={picTwo} alt="Some placeholder" className="img-fluid" />
                    </div>

                    <div className="col-md-6 pl-md-5">
                        <div className="form-volunteer">
                            <h2>Be A Volunteer Today</h2>

                            <form action="" method="POST" onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <input type="text" name='name' className="form-control py-2" id="name" placeholder="Enter your name" required/>
                                </div>
                                <div className="form-group">
                                    <input type="email" name='email' className="form-control py-2" id="email" placeholder="Enter your email" required/>
                                </div>
                                <div className="form-group">
                                    <textarea name="message" id="" cols="30" rows="3" className="form-control py-2" placeholder="Write your message" required></textarea>
                                </div>
                                <div className="form-group">
                                    <input type="submit" className="btn btn-white px-5 py-2" value="Send" />
                                </div>
                            </form>
                        </div>
                    </div>

                </div>
            </div>

            <Modal showModal={showModal} modalClose={modalClose} titleTxt="Данные о сообщении" 
                modalBody="Ваше письмо направлено в наш центр, мы с вами свяжемся скоро. Благодарим за обращение." />
        </div>
    );
}

export default BeVolunteerForm;
