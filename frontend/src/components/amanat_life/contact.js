import React, { useState } from 'react';
import { YMaps, Map } from 'react-yandex-maps';
import PageOverlay from './pageOverlay';
import Modal from './modal';

const Contact = () => {
    const [showModal, setShowModal] = useState(false);
    const modalClose = (bool) => { setShowModal(bool) };

    const handleSubmit = (e) => {
        e.preventDefault();
        const formData = new FormData(e.target);
        const formValDict = Object.fromEntries(formData);
        e.target.reset();
        setShowModal(true);
        // sending email to managers . . . (using formValDict)
    }
    return (
        <div className='contact-page-cls'>
            <PageOverlay overTitle="Get In Touch" bgImg="bg_2" extra={{ backgroundPosition: "50% 10%" }} />
            <div className="site-section">
                <div className="container">
                    <div className="row block-9">
                        <div className="col-md-6 pr-md-5">
                            <form action="" method='POST' onSubmit={handleSubmit}>
                                <div className="form-group">
                                    <input type="text" name='name' className="form-control px-3 py-3" placeholder="Your Name" required />
                                </div>
                                <div className="form-group">
                                    <input type="email" name='email' className="form-control px-3 py-3" placeholder="Your Email" required />
                                </div>
                                <div className="form-group">
                                    <input type="text" name='subject' className="form-control px-3 py-3" placeholder="Subject" />
                                </div>
                                <div className="form-group">
                                    <textarea name="message" id="" cols="30" rows="7" className="form-control px-3 py-3" placeholder="Message" required></textarea>
                                </div>
                                <div className="form-group">
                                    <input type="submit" value="Send Message" className="btn btn-primary py-3 px-5" />
                                </div>
                            </form>
                        </div>

                        <div className="col-md-6" id="map">
                            <YMaps>
                                <div>
                                    Мы живём тута ! ツ
                                    <Map defaultState={{ center: [55.75, 37.57], zoom: 9 }} width="100%" height={"70vh"} />
                                </div>
                            </YMaps>
                        </div>
                    </div>
                </div>
            </div>

            <Modal showModal={showModal} modalClose={modalClose} titleTxt="Данные о сообщении" 
                modalBody="Ваше письмо направлено в наш центр, мы с вами свяжемся скоро. Благодарим за обращение." />
        </div>
    );
}

export default Contact;