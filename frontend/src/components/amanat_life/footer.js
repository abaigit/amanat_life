import React from 'react';
import { Link } from 'react-router-dom';
import store from '../../store/store';

const Footer = () => {
    return (
        <footer className="footer">
            <div className="container">
                <div className="row mb-5">
                    <div className="col-md-6 col-lg-4">
                        <h3 className="heading-section">About Us</h3>
                        <p className="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. </p>
                        <p className="mb-5">Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                        <p><Link to="/about" className="link-underline">Read  More</Link></p>
                    </div>
                    <div className="col-md-6 col-lg-4">
                        <h3 className="heading-section">Recent Blog</h3>
                        {store.posts.length > 0 ?
                            store.posts.slice(0, 3).map(post => {
                                return (
                                    <div className="block-21 d-flex mb-4" key={post.id}>
                                        <figure className="mr-3">
                                            <img src={post.cover} alt="Footer pic" className="img-fluid" />
                                        </figure>
                                        <div className="text">
                                            <h3 className="heading"><Link to={`/blog/${post.id}`}>{post.title}</Link></h3>
                                            <div className="meta">
                                                <div><Link to="/"><span className="icon-calendar"></span> {new Date(post.timestamp).toLocaleDateString()}</Link></div>
                                                <div><Link to="/"><span className="icon-person"></span> {post.author.first_name}</Link></div>
                                                <div><Link to="/"><span className="icon-chat"></span> {post.comments.length}</Link></div>
                                            </div>
                                        </div>
                                    </div>
                                )
                            })

                            : null}
                    </div>
                    <div className="col-md-6 col-lg-4">
                        <div className="block-23">
                            <h3 className="heading-section">Get Connected</h3>
                            <ul>
                                <li><span className="icon icon-map-marker"></span><span className="text">203 Fake St. Mountain View, San Francisco, California, USA</span></li>
                                <li><Link to="/"><span className="icon icon-phone"></span><span className="text">+2 392 3929 210</span></Link></li>
                                <li><Link to="/"><span className="icon icon-envelope"></span><span className="text">info@yourdomain.com</span></Link></li>
                            </ul>
                        </div>
                    </div>


                </div>
                <div className="row pt-5">
                    <div className="col-md-12 text-center">
                        <p>
                            Copyright &copy;{new Date().getFullYear()} All rights reserved | This template is made with <i className="ion-ios-heart text-danger" aria-hidden="true"></i> by <a href="https://colorlib.com" target="_blank" >Colorlib</a>
                        </p>
                    </div>
                </div>
            </div>
        </footer>
    );
}

export default Footer;