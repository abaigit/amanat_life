import React from 'react';

const Modal = ({ showModal, modalClose, modalBody, titleTxt="Сообщение", titleType="text-info" }) => {
    const onClose = () => { modalClose(false) };
    const display = showModal ? "block" : "none";
    const modalCss = {
        display: display,
        width: "50%",
        margin: "auto",
        maxHeight: "45%",
        top: "10%",
    }

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={modalCss}>
            <div className="modal-dialog" role="document" style={{ maxWidth: "100%", height: "100%", margin: 0 }}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className={titleType + " modal-title"}><strong>{titleTxt}</strong></h5>
                        <button type="button" className="close" aria-label="Close" onClick={onClose}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div className="modal-body">
                        <h6>{modalBody}</h6>
                    </div>
                </div>
            </div>
        </div>
    )
}
 
export default Modal;