import React from 'react';
import { Link } from 'react-router-dom';


const DonatBoxItem = ({ box, cls }) => {
    const progressbar = (box.pledged / box.goal) * 100;
    const imageLinkCss = {
        height: "300px", 
        overflow: "hidden", 
        display: "block",
        backgroundImage: `url(${box.cover})`,
        backgroundPosition: "50%",
        backgroundRepeat: 'no-repeat',
        backgroundSize: 'cover',
    }

    return (
        <div className={ "card fundraise-item " + cls }>
            <Link to={`/box/${box.id}`} key={box.id} style={imageLinkCss}>
                {/* <img className="card-img-top" src={box.cover} alt="Some placeholder"/> */}
            </Link>
            <div className="card-body">
                <h3 className="card-title">
                    <Link to={`/box/${box.id}`} key={box.id}>
                        {box.title}
                    </Link>
                </h3>
                <p className="card-text">{box.headline.slice(0, 20)}</p>
                <span className="donation-time mb-3 d-block">Последнее пожертвование было 1д. назад</span>
                <div className="progress custom-progress-success">
                    <div className="progress-bar bg-primary" role="progressbar" style={{ width: `${progressbar}%` }} aria-valuenow="28" aria-valuemin="0" aria-valuemax="100"></div>
                </div>
                <span className="fund-raised d-block">{box.pledged} сом собрано из {box.goal} сом</span>
            </div>
        </div>
    );
}

export default DonatBoxItem;