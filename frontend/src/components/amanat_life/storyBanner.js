import React from 'react';
import { Link } from 'react-router-dom';
import store from '../../store/store';
import pic from '../../assets/img/img_1.jpg'


const StoryBanner = () => {
    const story = store.posts[0];
    return (
        <div className="featured-section overlay-color-2" style={{backgroundImage: `url(${pic})`}}>

            <div className="container">
                <div className="row">

                    <div className="col-md-6">
                        <img src={story.cover} alt="Some placeholder" className="img-fluid" />
                    </div>

                    <div className="col-md-6 pl-md-5">
                        <span className="featured-text d-block mb-3">Success Stories</span>
                        <h2>{story.title}</h2>
                        <div dangerouslySetInnerHTML={{ __html: story.content.slice(0, 250) }}></div>
                        <span className="fund-raised d-block mb-5">We have raised $100,000</span>
                        <p>
                            <Link to={"/blog/"+story.id} className="btn btn-success btn-hover-white py-3 px-5">
                                Read The Full Story
                            </Link>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default StoryBanner;