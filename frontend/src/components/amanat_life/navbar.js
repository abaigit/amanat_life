import React from 'react';
import { observer } from 'mobx-react';
import { NavLink } from 'react-router-dom';
import store from '../../store/store';

const Navbar = () => {
    return (
        <nav className="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light scrolled awake" id="ftco-navbar">
            <div className="container">
                <NavLink to="/"
                    className={({isActive}) => "navbar-brand nav-link" + (!isActive ? " " : " active")} >
                    AmanatLife
                </NavLink>

                <div className="collapse navbar-collapse" id="ftco-nav">
                    <ul className="navbar-nav ml-auto">
                        <li className="nav-item">
                            <NavLink to="/" 
                                className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                Главная
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/how-it-works"
                                className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                Как устроено
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/box/list"
                                className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                Проекты
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/blog"
                                className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                Блог
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/about"
                                className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                О нас
                            </NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink to="/contact"
                                className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                Контакт
                            </NavLink>
                        </li>

                        {
                            store.user.token
                                ? <>
                                    <li className="nav-item">
                                        <NavLink to="/account"
                                            className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                            Аккаунт
                                        </NavLink>
                                    </li>
                                    <li className="nav-item">
                                        <NavLink to="/login" onClick={()=>store.logout()}
                                            className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                            Выйти
                                        </NavLink>
                                    </li>
                                </>
                                :
                                <li className="nav-item">
                                    <NavLink to="/login"
                                        className={({isActive}) => "nav-link" + (!isActive ? " " : " active")} >
                                        Войти
                                    </NavLink>
                                </li>
                        }
                    </ul>
                </div>
            </div>
        </nav>
    );
}

export default observer(Navbar);
