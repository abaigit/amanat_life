import React from 'react';
import picOne from '../../assets/img/bg_1.jpg';
import picTwo from '../../assets/img/bg_2.jpg';


const PageOverlay = ({overTitle, bgImg, extra}) => {

    const styles = {
        image: {
            backgroundImage: bgImg == "bg_1" ? `url(${picOne})` : `url(${picTwo})`,
            backgroundRepeat: "no-repeat",
            backgroundSize: 'cover',
            backgroundPosition: "50%",
            ...extra,
        }
    }

    return (
        <div className="page-overlay-cls block-31" style={{position: "relative"}}>
            <div className="owl-carousel loop-block-31 ">
                <div className="block-30 block-30-sm item" style={styles.image} data-stellar-background-ratio="0.5">
                    <div className="container">
                        <div className="row align-items-center justify-content-center text-center">
                            <div className="col-md-7">
                                <h2 className="heading mb-5">{overTitle}</h2>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    );
}

export default PageOverlay;
