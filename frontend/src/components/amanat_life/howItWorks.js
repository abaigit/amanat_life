import React from 'react';
import BeVolunteerForm from './beVolunteerForm';
import PageOverlay from './pageOverlay';
import picOne from '../../assets/img/bg_1.jpg';
import picTwo from '../../assets/img/bg_2.jpg';

const HowItWorks = () => {
    return (
        <>
            <PageOverlay overTitle="How it Works" bgImg="bg_1"/>

            <div className="site-section">
                <div className="container">
                    <div className="row align-items-center mb-5">
                        <div className="col-md-7 order-md-2 mb-5 mb-md-0">
                            <img src={picOne} alt="" className="img-fluid" />
                        </div>
                        <div className="col-md-5 pr-md-5 mb-5 order-md-1">
                            <div className="block-41">
                                <div className="block-41-subheading d-flex">
                                    <div className="block-41-number">Step 01</div>
                                </div>
                                <h2 className="block-41-heading mb-3">Create Your Fundraising Campaign</h2>
                                <div className="block-41-text">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div className="row align-items-center mb-5">
                        <div className="col-md-7 mb-5 mb-md-0">
                            <img src={picTwo} alt="" className="img-fluid" />
                        </div>

                        <div className="col-md-5 pl-md-5 mb-5">
                            <div className="block-41">
                                <div className="block-41-subheading d-flex">
                                    <div className="block-41-number">Step 02</div>
                                </div>
                                <h2 className="block-41-heading mb-3">Share with Family and Friends</h2>
                                <div className="block-41-text">
                                    <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <BeVolunteerForm />
        </>
    );
}

export default HowItWorks;