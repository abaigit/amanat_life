import React from 'react';
import { observer } from 'mobx-react';
import store from '../../store/store';
import DonatBoxItem from './donatBoxItem';

const DonatBoxListPage = observer(() => {
    const flexCss = {
        display: "flex",
        flexDirection: "row",
        flexWrap: "wrap",
        alignContent: "flex-start",
        alignItems: "stretch",
        justifyContent: "space-between",
    }

    return (
        <div className='container py-5'>
            <div className="flex-container" style={flexCss}>
                {store.donationBoxes.map(item => {
                    return <DonatBoxItem box={item} key={item.id} cls={"col-4 mb-2"} />
                })}
            </div>

        </div>
    );
});

export default DonatBoxListPage;
