import React from 'react';
import { observer } from 'mobx-react';
import store from '../../store/store';

import { Swiper, SwiperSlide } from "swiper/react";
import { Pagination } from "swiper";
import "swiper/css";
import "swiper/css/pagination";

import DonatBoxItem from './donatBoxItem';


const DonatBoxSlider = observer(() => {

    return (
        <div style={{ paddingBottom: 10 }}>
            {
                store.donationBoxes
                    ? <Swiper
                        slidesPerView={3}
                        spaceBetween={30}
                        centeredSlides={true}
                        pagination={{
                            clickable: true
                        }}
                        modules={[Pagination]}
                        className="mySwiper"
                    >
                        {store.donationBoxes.map((box) => (
                            <SwiperSlide key={box.id}><DonatBoxItem box={box} /></SwiperSlide>
                        ))}
                    </Swiper>
                    : null
            }


        </div>
    )
})

export default DonatBoxSlider;