import React from 'react';

const OurMissionBlock = () => {
    return (
        <>
            <div className="site-section section-counter">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6 pr-5">
                            <div className="block-48">
                                <span className="block-48-text-1">Served Over</span>
                                <div className="block-48-counter ftco-number" data-number="1321901">0</div>
                                <span className="block-48-text-1 mb-4 d-block">Children in 150 Countries</span>
                                <p className="mb-0"><a href="/about" className="btn btn-white px-3 py-2">View Our Program</a></p>
                            </div>
                        </div>
                        <div className="col-md-6 welcome-text">
                            <h2 className="display-4 mb-3">Who Are We?</h2>
                            <p className="lead">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.</p>
                            <p className="mb-4">A small river named Duden flows by their place and supplies it with the necessary regelialia. </p>
                            <p className="mb-0"><a href="/about" className="btn btn-primary px-3 py-2">Learn More</a></p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="site-section border-top">
                <div className="container">
                    <div className="row">

                        <div className="col-md-4">
                            <div className="media block-6">
                                <div className="icon"><span className="ion-ios-bulb"></span></div>
                                <div className="media-body">
                                    <h3 className="heading">Our Mission</h3>
                                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                                    <p><a href="/about" className="link-underline">Learn More</a></p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="media block-6">
                                <div className="icon"><span className="ion-ios-cash"></span></div>
                                <div className="media-body">
                                    <h3 className="heading">Make Donations</h3>
                                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                                    <p><a href="/box/list" className="link-underline">Learn More</a></p>
                                </div>
                            </div>
                        </div>

                        <div className="col-md-4">
                            <div className="media block-6">
                                <div className="icon"><span className="ion-ios-contacts"></span></div>
                                <div className="media-body">
                                    <h3 className="heading">We Need Volunteers</h3>
                                    <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                                    <p><a href="/blog" className="link-underline">Learn More</a></p>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </>
    );
}

export default OurMissionBlock;
