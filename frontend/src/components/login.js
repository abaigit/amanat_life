import React, {useState} from 'react';
import { Form, Input, Button, Card } from 'antd';
import { useNavigate, Link } from "react-router-dom";
import store from '../store/store';

const LoginForm = ({ api }) => {
    const [showModal, setShowModal] = useState(false);
    const modalClose = (bool) => { setShowModal(bool) }
    let navigate = useNavigate();
    const onFinish = async (formData) => {
        let status = await store.authenticate(formData);
        if (status) {
            store.getUserData(formData.email);
            navigate("/")
        } else {
            setShowModal(true);
        }
    };

    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);  // TODO: show modal with Error message
    };

    return (
        <>
            <Card size={"small"} style={{ maxWidth: "450px", margin: "auto", marginBottom: 15 }}>
                <Form
                    name="login-form"
                    labelCol={{
                        span: 9,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="on"
                >

                    <Form.Item
                        label="Эл. почта"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Email!',
                            },
                        ]}
                    >
                        <Input type={'email'} />
                    </Form.Item>

                    <Form.Item
                        label="Пароль"
                        name="password"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                    >
                        <Button type="primary" htmlType="submit" className='btn btn-primary mr-3 my-3'>
                            Войти
                        </Button>
                        <Link to='/signup' className='text-info'>
                            Регистрация
                        </Link>
                    </Form.Item>
                </Form>
            </Card>
            
            <Modal showModal={showModal} modalClose={modalClose} />
        </>
    );
};


const Modal = ({ showModal, modalClose }) => {
    const onClose = () => { modalClose(false) };
    const display = showModal ? "block" : "none";
    const modalCss = {
        display: display,
        width: "50%",
        margin: "auto",
        maxHeight: "45%",
        top: "10%",
    }

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={modalCss}>
            <div className="modal-dialog" role="document" style={{ maxWidth: "100%", height: "100%", margin: 0 }}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title text-danger"><strong>Ошибка данных</strong></h5>
                        <button type="button" className="close" aria-label="Close" onClick={onClose}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div className="modal-body">
                        <h6>Неверные данные либо вы еще не активировали аккаунт</h6>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default LoginForm;
