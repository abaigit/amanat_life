import React from 'react';
import { Link } from 'react-router-dom';

const AccountActivated = () => {
    return ( 
        <div className='container my-5'>
            <h3 className='title'>
                Поздравляем, ваш Аккаунт активирован, Вы можете 
                <Link to="/login"> Войти</Link>
            </h3>
            
        </div>
     );
}
 
export default AccountActivated;
