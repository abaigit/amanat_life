import React, { useState } from 'react';
import { Form, Input, Button, Card } from 'antd';
import { csrftoken } from '../store/csrf_token';

const SignupForm = ({ api }) => {
    const [showModal, setShowModal] = useState(false);
    const [modalMsg, setModalMsg] = useState('');
    const modalClose = (bool) => { setShowModal(bool); setModalMsg('') }
    const [form] = Form.useForm();
    const onFinish = async (formData) => {
        let res = await fetch(`${api}/account/signup/`, {
            method: 'POST',
            body: JSON.stringify(formData),
            headers: {
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
                'X-CSRFToken': csrftoken,
            },
            // mode: 'same-origin',
        })
        if (res.ok) {
            let json = await res.json();
            form.resetFields();
            setModalMsg("Вы зарегестрированы, пройдите активацию в почте");
            setShowModal(true);
        } else {
            setModalMsg("Пользователь с таким email или телефоном уже имеется");
            setShowModal(true);
        }
    };

    const onFinishFailed = (errorInfo) => {
        setModalMsg(errorInfo);
        setShowModal(true);
    };

    return (
        <>
            <Card size={"small"} style={{ maxWidth: "450px", margin: "auto", marginBottom: "15px" }}>
                <Form
                    form={form}
                    name="signup-form"
                    labelCol={{
                        span: 9,
                    }}
                    wrapperCol={{
                        span: 14,
                    }}
                    initialValues={{
                        remember: true,
                    }}
                    onFinish={onFinish}
                    onFinishFailed={onFinishFailed}
                    autoComplete="on"
                >
                    <Form.Item
                        label="Имя"
                        name="first_name"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your First Name!',
                            },
                        ]}
                        className="mb-4"
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Фамилия"
                        name="last_name"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Last Name!',
                            },
                        ]}
                        className="mb-4"
                    >
                        <Input />
                    </Form.Item>

                    <Form.Item
                        label="Эл. почта"
                        name="email"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Email!',
                            },
                        ]}
                        className="mb-4"
                    >
                        <Input type={'email'} />
                    </Form.Item>

                    <Form.Item
                        label="Телефон"
                        name="phone_number"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Email!',
                            },
                        ]}
                        className="mb-4"
                    >
                        <Input type={'number'} />

                    </Form.Item>

                    <Form.Item
                        label="Дата рождения"
                        name="birth_date"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your Date of birth!',
                            },
                        ]}
                        className="mb-4"
                    >
                        <Input type={'date'} />

                    </Form.Item>

                    <Form.Item
                        label="Пароль"
                        name="password1"
                        rules={[
                            {
                                required: true,
                                message: 'Please input your password!',
                            },
                        ]}
                        className="mb-4"
                    >
                        <Input.Password />
                    </Form.Item>

                    <Form.Item
                        label="Пароль (подтвертдить)"
                        name="password2"
                        rules={[
                            {
                                required: true,
                                message: 'Please confirm your password!',
                            },
                        ]}
                        className="mb-4"
                    >
                        <Input.Password />
                    </Form.Item>


                    <Form.Item
                        wrapperCol={{
                            offset: 8,
                            span: 16,
                        }}
                        className="mb-4"
                    >
                        <Button type="primary" htmlType="submit" className='btn btn-primary mt-3'>
                            Зарегистрировать
                        </Button>
                    </Form.Item>
                </Form>
            </Card>

            <Modal showModal={showModal} modalClose={modalClose} message={modalMsg} />
        </>
    );
};


const Modal = ({ showModal, modalClose, message }) => {
    const onClose = () => { modalClose(false) };
    const display = showModal ? "block" : "none";
    const modalCss = {
        display: display,
        width: "50%",
        margin: "auto",
        maxHeight: "45%",
        top: "10%",
    }

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={modalCss}>
            <div className="modal-dialog" role="document" style={{ maxWidth: "100%", height: "100%", margin: 0 }}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title text-info"><strong>Регистрация</strong></h5>
                        <button type="button" className="close" aria-label="Close" onClick={onClose}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div className="modal-body">
                        <h6>{message}</h6>
                    </div>
                </div>
            </div>
        </div>
    )
}


export default SignupForm;