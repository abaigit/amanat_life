import React from 'react';
import { Link } from 'react-router-dom';

const NotFound = () => {
    return ( 
        <div className='container my-5'>
            <h4>Упс, такой странички нет</h4>
            <h5><Link to="/">Может Домой ?</Link></h5>
        </div>
     );
}
 
export default NotFound;
