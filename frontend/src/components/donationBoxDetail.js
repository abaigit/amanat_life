import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
import { observer } from 'mobx-react';
import { Image, Progress, Form, Input } from 'antd';
import { csrftoken } from '../store/csrf_token';
import store from '../store/store';

const DonationBoxDetail = observer(({ api }) => {
    const [showModal, setShowModal] = useState(false);
    const { id: box_id } = useParams();
    const box = store.donationBoxes.find(item => item.id == box_id)
    const pledgeStatus = ((box.pledged / box.goal) * 100).toFixed(2);
    const onDonateBtnClick = () => { setShowModal(true) }
    const handClose = (bool) => { setShowModal(bool) }

    return (
        <div className='container'>
            <h3 style={{ borderBottom: "1px solid #0000001a", marginBottom: 15 }}>{box.title}</h3>
            <div className="content">
                <div className="row">
                    <div className="col col-5">
                        <Image width={"100%"} src={box.cover} className="w-100" />
                    </div>
                    <div className="col col-7">
                        <h5>Получатель: {box.recipient}</h5>
                        <h5>Причина: {box.reason}</h5>
                        <div><h5>Описание: </h5><div dangerouslySetInnerHTML={{ __html: box.description }}></div></div>
                        <h5>Собрано: {box.pledged} сом   (из {box.goal})</h5>
                        <Progress
                            strokeColor={{
                                from: '#108ee9',
                                to: '#87d068',
                            }}
                            percent={pledgeStatus}
                            status="active"
                        />
                        <PaymentModal show={showModal} api={api} handClose={handClose} recipient={box.recipient} box={box} />
                        <button className='btn btn-primary my-3' onClick={onDonateBtnClick}>Пожертвовать</button>
                    </div>
                </div>
            </div>
        </div>
    );
});

const PaymentModal = ({ show, handClose, recipient, box:dBox, api }) => {
    const [form] = Form.useForm();
    const display = show ? "block" : "none";
    const onClose = () => { handClose(false) };
    const onPay = (formData) => {
        let {amount, box=dBox.id} = formData;
        fetch(`${api}/crowdfund/payment/`, {
            method: 'POST',
            body: JSON.stringify({amount, box, check_details: formData}),
            headers: {
                "Content-Type": "application/json; charset=utf8",
                'Accept': 'application/json',
                'X-CSRFToken': csrftoken,
            },
        }).then(res=>res.json()).then(data=>store.getDonationBoxes());
        form.resetFields();
        onClose();
    }
    const modalCss = {
        display: display,
        width: "80%",
        left: "auto",
        right: "auto",
        marginLeft: "-34%",
        maxHeight: "75%",
        top: "10%",
        overflowY: "scroll"
    }

    return (
        <div className="modal" tabIndex="-1" role="dialog" style={modalCss}>
            <div className="modal-dialog" role="document" style={{ maxWidth: "100%", height: "100%", margin: 0 }}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h5 className="modal-title">Оплатить пожертвование для <strong>{dBox.title}</strong></h5>
                        <button type="button" className="close" aria-label="Close" onClick={onClose}>
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>

                    <div className="modal-body">
                        <Form
                            form={form}
                            name="donation-form"
                            labelCol={{
                                span: 9,
                            }}
                            wrapperCol={{
                                span: 14,
                            }}
                            initialValues={{
                                recipient: recipient,
                            }}
                            onFinish={onPay}
                            onFinishFailed={() => console.log("not finis")}
                            autoComplete="on"
                        >
                            <Form.Item
                                className='mb-4'
                                label="Ваше Ф.И.О"
                                name="fullname"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Пожалуйста введите ваше Имя Фамилия!',
                                    },
                                ]}
                            >
                                <Input className="form-control" />
                            </Form.Item>

                            <Form.Item
                                className='mb-4'
                                label="Номер карты"
                                name="cardNum"
                                rules={[
                                    {
                                        required: true,
                                        message: 'введите 12 значный номер',
                                    },
                                ]}
                            >
                                <Input className="form-control" type="number"/>
                            </Form.Item>

                            <Form.Item
                                className='mb-4'
                                label="Сумма доната"
                                name="amount"
                                rules={[
                                    {
                                        required: true,
                                        message: 'Укажите пожалуйста сумму',
                                    },
                                ]}
                            >
                                <Input className="form-control" type="number"/>
                            </Form.Item>

                            <Form.Item
                                className='mb-4'
                                label="Получатель"
                                name="recipient"
                                rules={[
                                    {
                                        required: true,
                                    },
                                ]}
                            >
                                <Input className="form-control" disabled/>
                            </Form.Item>

                            <button type='button' className="btn btn-secondary text-light mt-3 float-right" onClick={onClose}>Закрыть</button>
                            
                            <Form.Item>
                                <button className="btn btn-primary float-right mt-3 mr-3">Оплатить</button>
                            </Form.Item>
                        </Form>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default DonationBoxDetail;
