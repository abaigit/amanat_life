import React from 'react';
import { useParams } from 'react-router-dom';
import { observer } from 'mobx-react';
import store from '../../store/store';
import PageOverlay from '../amanat_life/pageOverlay';
import person from '../../assets/img/person_1.jpg';
import PostCommentList from './PostComment';
import PostCommentForm from './PostCommentForm';

const BlogPostDetails = observer(() => {
    const { id: post_id } = useParams();
    const post = store.posts.find(item => item.id == post_id)
    const onCommentFormSubmit = (e) => {
        e.preventDefault();
        let dataDict = {
            content: e.target.message.value,
            timestamp: new Date().toISOString(),
            object_id: post.id,
        }
        store.makeComment(dataDict);
        e.target.reset();
    }

    return (
        <div>
            <PageOverlay overTitle={post.title} bgImg="bg_1" />

            <div id="blog" className="site-section">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8">
                            <p className="mb-4">
                                <img src={post.cover} alt="cover" className="img-fluid" />
                            </p>
                            <div dangerouslySetInnerHTML={{ __html: post.content }}></div>
                            <div className="tag-widget post-tag-container mb-5 mt-5">
                                <div className="tagcloud">
                                    {
                                        post.tags.map(tag => <a href="/" className="tag-cloud-link" key={tag.id + 3 * 2}>{tag.name}</a>)
                                    }
                                </div>
                            </div>
                            {post.comments ? <PostCommentList comments={post.comments} key={post.id} /> : null}
                            
                            {
                                store.user.token
                                    ? <PostCommentForm onSubmit={onCommentFormSubmit} /> :
                                    <h5>Только авторизованные могут оставлять комментарий</h5>
                            }

                        </div>

                        <div className="col-md-4 sidebar">
                            <div className="sidebar-box">
                                <form action="/" className="search-form">
                                    <div className="form-group">
                                        <span className="icon fa fa-search"></span>
                                        <input type="text" className="form-control" placeholder="Type a keyword and hit enter" />
                                    </div>
                                </form>
                            </div>
                            <div className="sidebar-box">
                                <div className="categories">
                                    <h3>Categories</h3>
                                    <li><a href="/">Charity <span>(12)</span></a></li>
                                    <li><a href="/">Donations <span>(22)</span></a></li>
                                    <li><a href="/">News <span>(37)</span></a></li>
                                    <li><a href="/">Updates <span>(42)</span></a></li>
                                </div>
                            </div>
                            <div className="sidebar-box">
                                <img src={person} alt="Some pic placeholder" className="img-fluid mb-4 rounded" />
                                <h3>About The Author</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
                                <p><a href="/" className="btn btn-primary btn-lg">Read More</a></p>
                            </div>

                            <div className="sidebar-box">
                                <h3>Tag Cloud</h3>
                                <div className="tagcloud">
                                    {
                                        post.tags.map(tag => <a href="/" className="tag-cloud-link" key={tag.id}>{tag.name}</a>)
                                    }

                                </div>
                            </div>

                            <div className="sidebar-box">
                                <h3>Paragraph</h3>
                                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus itaque, autem necessitatibus voluptate quod mollitia delectus aut, sunt placeat nam vero culpa sapiente consectetur similique, inventore eos fugit cupiditate numquam!</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
});

export default BlogPostDetails;
