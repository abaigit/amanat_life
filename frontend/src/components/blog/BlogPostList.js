import React from 'react';
import { Link } from 'react-router-dom';
import { observer } from 'mobx-react';
import store from '../../store/store';

const BlogPostListItem = ({ post }) => {

    return (
        <div className="col-12 col-sm-6 col-md-6 col-lg-4 mb-4 mb-lg-0">
            <div className="post-entry">
                <Link to={`/blog/${post.id}`} className="mb-3 img-wrap">
                    <img src={post.cover} alt="Some pic" className="img-fluid" />
                </Link>

                <h3>
                    <Link to={`/blog/${post.id}`} className="mb-1">
                        {post.title}
                    </Link>
                </h3>
                <span className="date mb-4 d-block text-muted">{new Date(post.timestamp).toDateString()}</span>
                <div dangerouslySetInnerHTML={{ __html: post.content.slice(0, 120) }}></div>
                <p>
                    <Link to={`/blog/${post.id}`} className="link-underline">
                        Read More
                    </Link>
                </p>
            </div>
        </div>
    );
}

const BlogPostList = observer(() => {
    return (
        <>
            <div className="site-section bg-light">
                <div className="container">
                    <div className="row">
                        {store.posts ? store.posts.map(post => <BlogPostListItem post={post} key={post.id} />) : null}
                    </div>
                </div>
            </div>
        </>
    );
})

export default BlogPostList;
