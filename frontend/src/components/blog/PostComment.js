import React from 'react';
import person from '../../assets/img/person_1.jpg';

const PostComment = ({comment}) => {
    return (
        <li className="comment">
            <div className="vcard bio">
                <img src={person} alt="Person pic" />
            </div>
            <div className="comment-body">
                <h3>{comment.author.first_name} {comment.author.last_name}</h3>
                <div className="meta">{new Date(comment.timestamp).toDateString(    )}</div>
                <div dangerouslySetInnerHTML={{ __html: comment.content }}></div>
                <p><a href="/" className="reply">Reply</a></p>
            </div>
        </li>
    );
}

const PostCommentList = ({comments}) => {
    return (
        <div className="pt-5 mt-5">
            <h3 className="mb-5">{comments.length} Comments</h3>
            <ul className="comment-list">
                {comments.map(comment=><PostComment comment={comment} key={comment.id}/>)}
            </ul>
        </div>
    );
}

export default PostCommentList;
