import React from 'react';

const PostCommentForm = ({onSubmit}) => {
    return (
        <div className="comment-form-wrap pt-5">
            <h3 className="mb-5">Leave a Comment</h3>
            <form action="#" className="" id='commentForm' onSubmit={(e)=>onSubmit(e)}>
                {/* <div className="form-group">
                    <label htmlFor="name">Name *</label>
                    <input type="text" className="form-control" id="name"  required/>
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email *</label>
                    <input type="email" className="form-control" id="email" required/>
                </div>
                <div className="form-group">
                    <label htmlFor="website">Website</label>
                    <input type="url" className="form-control" id="website" required/>
                </div> */}

                <div className="form-group">
                    <label htmlFor="message">Message</label>
                    <textarea name="content" id="message" cols="20" rows="6" className="form-control" required></textarea>
                </div>
                <div className="form-group">
                    <input type="submit" value="Post Comment" className="btn py-3 px-4 btn-primary"/>
                </div>
            </form>
        </div>
    );
}

export default PostCommentForm;