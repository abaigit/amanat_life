import './assets/fonts/dosis.css'
import './assets/bootstrap.css';
import './assets/icomoon.css';
import './assets/style.css';
import GIF from './assets/img/onload.gif';

import React, { useState, useEffect } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';

import SignupForm from "./components/signup";
import LoginForm from './components/login';
import NotFound from './components/404';
import AccountActivated from './components/accountActivated';
import AccountUpdateForm from './components/accountDetails';

// AMANAT LIFE
import Navbar from './components/amanat_life/navbar';
import Footer from './components/amanat_life/footer';
import Home from './components/amanat_life/Home';
import HowItWorks from './components/amanat_life/howItWorks';
import About from './components/amanat_life/about';
import Contact from './components/amanat_life/contact';
import DonatBoxListPage from './components/amanat_life/donatBoxListPage';
import DonationBoxDetail from './components/donationBoxDetail';

// BLOG 
import BlogPostDetails from './components/blog/BlogPostDetails';
import BlogPostList from './components/blog/BlogPostList';

import store from './store/store';

const API = process.env.REACT_APP_API

const App = () => {
  const [isFetching, setIsFetching] = useState(true);

  useEffect(() => {
    async function fetchData() {
      let res = await store.getDonationBoxes();
      let pres = await store.getPosts();
      setIsFetching(!res);
    }
    fetchData();
  }, []);


  const OnFetching = () => {
    const onloadStyle = {
      backgroundImage: `url(${GIF})`,
      backgroundSize: 'contain',
      backgroundPosition: "50%",
      backgroundRepeat: 'no-repeat',
      position: 'fixed',
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      width: "100%",
      height: "100%",
    }

    return (
      <div className='container' style={onloadStyle}></div>
    )
  }

  const AppRootComp = () => {
    return (
      <Router>
        <Navbar />
        <Routes>

          <Route exact path="/" element={<Home />} />
          <Route path="/signup" element={<SignupForm api={API} />} />
          <Route path="/activated" element={<AccountActivated />} />
          <Route path="/login" element={<LoginForm api={API} />} />
          <Route path="/account" element={<AccountUpdateForm api={API} />} />
          <Route path="/how-it-works" element={<HowItWorks />} />
          <Route path="/blog" element={<BlogPostList />} />
          <Route path="/blog/:id" element={<BlogPostDetails />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
          <Route path="/box/list/" element={<DonatBoxListPage />} />
          <Route path="/box/:id" element={<DonationBoxDetail api={API} />} />
          <Route path="*" element={<NotFound />} />

        </Routes>
        <Footer />
      </Router>
    );
  }

  if (isFetching) {
    return <OnFetching />
  } else {
    return <AppRootComp />
  }

}

export default App;
