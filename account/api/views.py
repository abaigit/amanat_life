from django.shortcuts import redirect, HttpResponse
from django.contrib.auth import get_user_model
from django.contrib.sites.shortcuts import get_current_site
from django.utils.decorators import method_decorator
from django.utils import timezone
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_decode
from django.contrib.auth.tokens import default_token_generator
from rest_framework.generics import ListAPIView, CreateAPIView
from rest_framework.views import APIView, View
from rest_framework.response import Response
from rest_framework import exceptions
from rest_framework.decorators import api_view, renderer_classes, permission_classes
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated
from account.tasks import celery_send_conf_mail
from .serializers import (UserBaseSerializer, UserSignupSerializer,
                          UserPasswordChangeSerializer)

User = get_user_model()


@api_view(http_method_names=['POST'])
def signup_view(request):
    rdata = request.data
    serialized = UserSignupSerializer(data=rdata)
    if serialized.is_valid(raise_exception=True):
        user = serialized.save()
        domain = str(get_current_site(request).domain)
        celery_send_conf_mail.delay("Регистрация на Charity Globe",
                                    'account/signup_confirm_email.html', user.id, domain)
        return Response({"success": "confirmation email is sent"})
    else:
        return Response({"failure": serialized.error_messages})


def activate_user(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = User.objects.get(pk=uid)
    except(TypeError, ValueError, OverflowError, User.DoesNotExist):
        user = None
    if user is not None and default_token_generator.check_token(user, token):
        user.is_active = True
        user.last_login = timezone.now()  # чтобы токен сбросился
        user.save()
        # return redirect('/activated/')
        return HttpResponse("<h2>Activated</h2>")
    # return redirect("/not-activated/")
    return HttpResponse("<h2>NOT Activated</h2>")


@api_view(http_method_names=["POST"])
# @permission_classes([IsAuthenticated])
def account_detail(request):
    rdata = request.data
    try:
        user = User.objects.get(email=rdata.get("email"))
    except User.DoesNotExist:
        raise exceptions.NotFound
    user = UserBaseSerializer(instance=user)
    return Response(user.data)


@api_view(http_method_names=["PATCH", "PUT"])
@permission_classes([IsAuthenticated])
def account_update(request):
    rdata = request.data
    try:
        # user = User.objects.get(email=rdata.get("email"))
        user = request.user  # because it authenticates via JWT
    except User.DoesNotExist:
        raise exceptions.NotFound
    serialized = UserBaseSerializer(data=rdata, partial=True, instance=user)
    if serialized.is_valid(raise_exception=True):
        serialized.save()
    return Response(serialized.data)


@api_view(http_method_names=["PATCH", "PUT"])
@permission_classes([IsAuthenticated])
def password_update(request):
    rdata = request.data
    sr = UserPasswordChangeSerializer(instance=request.user, partial=True, data=rdata)
    if sr.is_valid(raise_exception=True):
        sr.save(new_password=sr.validated_data.get("password_new1"))
    return Response(sr.data)
