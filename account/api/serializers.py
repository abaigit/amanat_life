from django.contrib.auth import get_user_model
from rest_framework import serializers
from rest_framework.exceptions import ValidationError

User = get_user_model()


class UserBaseSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('email', 'first_name', 'last_name', 'position', 'birth_date', 'phone_number')
        extra_kwargs = {
            "email": {"read_only": True}
        }


class UserSignupSerializer(serializers.ModelSerializer):
    password1 = serializers.CharField(write_only=True, style={'input_type': 'password'}, )
    password2 = serializers.CharField(write_only=True, style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email', 'phone_number', 'birth_date', 'password1', 'password2')

    def create(self, validated_data):
        user_data = dict([(key, val) for key, val in validated_data.items() if key not in ['password1', 'password2']])
        user = User.objects.create(**user_data)
        user.set_password(validated_data.get("password1"))
        user.save()
        return user

    def validate(self, data):
        email = data['email']
        password1 = data['password1']
        password2 = data['password2']
        user_qs = User.objects.filter(email=email).distinct()
        if password1 != password2:
            raise ValidationError("Passwords don't match")
        elif user_qs.count() > 0:
            raise ValidationError("Such user already exists")
        return data


class UserPasswordChangeSerializer(serializers.ModelSerializer):
    password_new1 = serializers.CharField(write_only=True, style={'input_type': 'password'})
    password_new2 = serializers.CharField(write_only=True, style={'input_type': 'password'})

    class Meta:
        model = User
        fields = ("password", "password_new1", "password_new2")
        extra_kwargs = {
            "password": {"write_only": True}
        }

    def validate(self, data):
        origin_password = data.get("password")
        password1 = data.get("password_new1")
        password2 = data.get("password_new2")

        if self.instance.check_password(origin_password) is False:
            raise ValidationError("OLD PASSWORD WRONG")
        elif password1 != password2:
            raise ValidationError("PASSWORDS DON'T MATCH")
        return data

    def save(self, *args, **kwargs):
        new_password = kwargs.get("new_password")
        if new_password is None:
            raise ValueError("No password was provided")
        self.instance.set_password(new_password)
        self.instance.save()
        return self.instance
