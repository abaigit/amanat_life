from django.db import models
from django.utils.translation import gettext_lazy as _
from django.core.validators import RegexValidator
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)

phone_regex = RegexValidator(regex=r'\b0[0-9]{9}\b',
                             message="Номер Телефона должен быть в формате: '0123123456'. 10 символов.")


class UserManager(BaseUserManager):
    use_in_migrations = True

    # def _create_user(self, email, password, **extra_fields):
    """
    Creates and saves a User with the given email and password.
    """

    def create_user(self, email, password=None, **extra_fields):
        # extra_fields.setdefault('is_superuser', False) # po idee i tak false je
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password, **extra_fields):
        extra_fields.setdefault('is_superuser', True)
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_active', True)

        # if extra_fields.get('is_superuser') is False:
        #     raise ValueError('Superuser must have is_superuser=True.')

        return self.create_user(email, password, **extra_fields)


POSITIONS = (
    ('PT', 'patient'),
    ('SP', 'sponsor'),
    ('MN', 'Manager'),
    ('HD', 'Headship'),
)


class User(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField(
        verbose_name='Email',
        max_length=255,
        unique=True,
    )
    # TODO: get rid of null=True etc. in prod
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    birth_date = models.DateField(null=True, blank=True)
    phone_number = models.CharField(max_length=15, validators=[phone_regex], unique=True)
    position = models.CharField(max_length=4, choices=POSITIONS, default='SP')
    is_active = models.BooleanField(default=False)
    is_staff = models.BooleanField(default=False)

    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['phone_number']

    class Meta:
        verbose_name = _('user')
        verbose_name_plural = _('users')

    def __str__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    def get_fullname(self):
        return f"{self.first_name} {self.last_name}"
