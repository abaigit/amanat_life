from django.contrib import admin
from django.contrib.auth.models import Group
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin
from .forms import UserAdminChangeForm, UserAdminCreationForm
from .models import User


@admin.register(User)
class UserAdmin(BaseUserAdmin):
    # The forms to add and change user instances
    form = UserAdminChangeForm
    add_form = UserAdminCreationForm

    # The fields to be used in displaying the User model.
    # These override the definitions on the base UserAdmin
    # that reference specific fields on auth.User.
    list_display = ('email', 'first_name', 'phone_number', 'position', 'is_superuser')
    list_filter = ('is_superuser', 'is_active')
    fieldsets = (
        (None,              {'fields': ('email', 'password'), 'classes': ('wide',)}),
        ('Personal info',   {'fields': ('first_name', 'last_name', 'phone_number', 'birth_date', 'last_login')}),
        ('Permissions',     {'fields': ('position', 'is_superuser', 'is_active', 'user_permissions')}),
    )
    readonly_fields = ('last_login',)
    # add_fieldsets is not a standard ModelAdmin attribute. UserAdmin
    # overrides get_fieldsets to use this attribute when creating a user.
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'last_name', 'phone_number', 'password1', 'password2')}
         ),
    )
    search_fields = ('email', 'first_name', 'last_name', 'phone_number')
    ordering = ('-id',)
    filter_horizontal = ()


admin.site.unregister(Group)
