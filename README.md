## Backend
### `Django 3.2`
In the root directory, you can run: `python manage.py runserver`


## Frontend
### `React ^17.0.0`

In the `frontend` directory, you can run: `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.


## Distributed Task Queue
### `Celery 5.2.3`
After running server, activate worker with command: <br/>
`celery -A server worker -l info`<br/>
Celery Beat: <br/>
`celery -A server woker --beat -l info`<br/>


## Learn More

You can learn more about [Django documentation](https://www.djangoproject.com/).

To learn React, check out the [React documentation](https://reactjs.org/).

To learn about Celery, check out the [Celery documentation](https://docs.celeryq.dev/en/stable/).

To learn about Redis, check out the [Redis](https://redis.io/).
